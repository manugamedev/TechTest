const express = require('express')
//const  http = require('http')

let notes = [
    {
        "id": 1,
        "content": "Contenido de la nota 1 aa",
        "date": "2022-05-18T11:59:07",
        "important": true
    },
    {
        "id": 2,
        "content": "Contenido de la nota 2",
        "date": "2022-05-14T18:05:07",
        "important": false
    },
    {
        "id": 3,
        "content": "Contenido de la nota 3",
        "date": "2022-04-07T09:48:07",
        "important": true
    }
]

// const app = http.createServer((request, response) => 
// {
//     response.writeHead(200, {'Content-Type': 'application/json'})
//     response.end(JSON.stringify(notes))
// })

const app = express()

app.get('/', (request, response) => {
    response.send(`<h1>Hello World, requested at: ${new Date().toLocaleString('es-ES')}</h1>`)
})

app.get('/api/notes', (request, response) => {
    response.json({notes})
})

app.get('/api/notes/:id', (request, response) => {
    const id = Number(request.params.id)
    const note = notes.find(n => n.id === id)

    if (note){
        response.json(note)
    } else {
        response.status(404).end()
    }
})

app.delete('/api/notes/:id', (request, response) => {
    const id = Number(request.params.id)
    notes = notes.filter(n => n.id !== id)

    response.status(204).end()
})

app.post('/api/notes', (request, response) => {
    const note = request.content

    if (!note || !note.content){
        return response.status(400).json({
            error: 'note content is missing'
        })
    }

    const ids = notes.map(note => note.id)
    const maxId = Math.max(ids)

    const newNote = {
        id: maxId + 1,
        content: note.content,
        important: typeof note.important !== 'undefined' ? note.important : false,
        date: new Date().toLocaleString('es-ES')
    }

    notes = [...notes, newNote]

    response.status(201).json(newNote)
})

app.get('/api/date', (request, response) => {
    response.json({"date": new Date()})
})

const PORT = 3001
app.listen(PORT), () => {
    console.log(`Server is running on port ${PORT}`)
}