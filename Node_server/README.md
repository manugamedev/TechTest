# TechTest



## Node Server

Para levantar el servidor de node que utiliza express, lo único que habrá que hacer es situarse en la consola de comandos en la carpeta del proyecto "Node_server" y ejecutar el comando npm run start_express. Para comprobar que está correctamente levantado, podemos abrir una pestaña en un navegador, indicar la URL localhost:3001 (el puerto por defecto asignado es el 3001) y ver que nos devuelve un mensaje de bienvenida.

## Proyecto de Unity

Está estructurado de forma muy sencilla, todo en un único script (por facilidad y velocidad de resolución) y en un proyecto 2D con simplemente un canvas y los elementos requeridos para la prueba. Se ha añadido un .gitignore al proyecto para evitar la sobrecarga de ficheros en el repositorio.