# TechTest

## Node Server

Para levantar el servidor de node que utiliza express, lo único que habrá que hacer es situarse en la consola de comandos en la carpeta del proyecto "Node_server" y ejecutar el comando npm run start_express. Para comprobar que está correctamente levantado, podemos abrir una pestaña en un navegador, indicar la URL localhost:3001 (el puerto por defecto asignado es el 3001) y ver que nos devuelve un mensaje de bienvenida.

## Proyecto de Unity

El proyecto de Unity está estructurado básicamente en 2 carpetas:
- Scenes: Que contiene la escena donde se sitúan todos los elementos requeridos en la prueba.
- Scripts: Contiene los scripts con la lógica del proyecto. Que por rapidez, sólo contiene uno.

Si al cargar la primera vez el proyecto de Unity, aparece una escena en blanco, se debe ir a la carpeta Scenes y abrir la escena que ésta contiene, de esta forma cargaremos la escena correspondiente.

## Repo

Se ha incluído en el repositorio un .gitignore para evitar la sobrecarga de ficheros y optimizar el espacio en el repositorio.