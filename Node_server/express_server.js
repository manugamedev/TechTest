const express = require('express')

const app = express()

const PORT = 3001
app.listen(PORT), () => {
    console.log(`Server is running on port ${PORT}`)
}

app.get('/', (request, response) => {
    response.send('Bienvenid@ al servidor CurrentDate');
})

app.get('/api/date', (request, response) => {
    const date = new Date();
    response.json({"date": new Date()})
})

app.get('/api/dateformatted', (request, response) => {
    response.send(new Date().toLocaleDateString())
})