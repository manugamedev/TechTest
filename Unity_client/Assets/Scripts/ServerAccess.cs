using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ServerAccess : MonoBehaviour
{
    public Text DateText;
    public GameObject popupError;

    private UnityWebRequest response;

    public UnityWebRequest GetDateFromServer()
    {
        StartCoroutine(GetDateFromServerRequest());

        return response;
    }

    public void OnClickBtnGetDate()
    {
        UnityWebRequest wr = GetDateFromServer();

        if (string.IsNullOrWhiteSpace(wr.error))
        {
            Debug.Log(wr.downloadHandler.text);
            DateText.text = wr.downloadHandler.text;
        }
        else
        {
            StartCoroutine(showPopupError(wr.error));
        }
    }

    public void OnClickAcceptError()
    {
        popupError.SetActive(false);
    }

    private IEnumerator GetDateFromServerRequest()
    {
        UnityWebRequest wr = UnityWebRequest.Get("localhost:3001/api/dateformatted");

        response = wr;

        UnityWebRequestAsyncOperation asyncOp = wr.SendWebRequest();

        asyncOp.completed += OnCompleteDateRequest;

        yield return new WaitUntil(() => asyncOp.isDone);
    }

    private void OnCompleteDateRequest(AsyncOperation op)
    {
        if (string.IsNullOrWhiteSpace(response.error))
        {
            Debug.Log(response.downloadHandler.text);
            DateText.text = response.downloadHandler.text;
        }
        else
        {
            StartCoroutine(showPopupError(response.error));
        }
    }

    private IEnumerator showPopupError(string errorMsg)
    {
        if (popupError != null)
        {
            popupError.SetActive(true);
            Text textError = popupError.GetComponentInChildren<Text>();
            textError.text = errorMsg;
        }
        else
        {
            yield return new WaitForSeconds(0);
        }
    }
}
